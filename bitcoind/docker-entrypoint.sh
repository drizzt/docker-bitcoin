#!/bin/sh
set -ex

case "$1" in
  -*)
    echo "$0: assuming arguments for bitcoind"

    set -- bitcoind "$@"
    ;;
esac

if [ "$1" = "bitcoind" ]; then
  mkdir -p "/home/bitcoin/.bitcoin"
  chmod 700 "/home/bitcoin/.bitcoin"
  chown -R bitcoin "/home/bitcoin/.bitcoin"

  if [ ${ROOTLESS:-0} -eq 0 ]; then
    if [ -z "$BITCOIND_TOR_IP" ]; then
      BITCOIND_TOR_IP=$(dig +short bitcoind_tor)
    fi
    CURLOPTS="--socks5-hostname $BITCOIND_TOR_IP:9050"
  else
    BITCOIND_TOR_IP=127.0.0.1
  fi

  until curl -so /dev/null http://$BITCOIND_TOR_IP:9052; do
    sleep 1
  done

  echo "$0: setting data directory to /home/bitcoin/.bitcoin"

  touch "/home/bitcoin/.bitcoin/bitcoin.conf"
##  sed -i '/^rpcauth=.*/d' "/home/bitcoin/.bitcoin/bitcoin.conf"
##  salt=$(openssl rand -hex 16)
##  echo -n "$BITCOIND_RPCPASS" | openssl dgst -sha256 -hmac "$salt" | sed "s;^.* ;rpcauth=rpcuser:$salt\$;" >> "/home/bitcoin/.bitcoin/bitcoin.conf"

  set -- "$@" -bind=0 -debug=tor -listen=1 -proxy="$BITCOIND_TOR_IP:9050" -rpcallowip=0.0.0.0/0 -rpcbind=0.0.0.0 -torcontrol="$BITCOIND_TOR_IP:9052" -torpassword= -zmqpubrawblock=tcp://0.0.0.0:28332 -zmqpubrawtx=tcp://0.0.0.0:28333
fi

echo
if [ "$1" = "bitcoind" ] || [ "$1" = "bitcoin-cli" ] || [ "$1" = "bitcoin-tx" ]; then
  NEW_BITCOIN_VERSION=$(curl --silent $CURLOPTS "https://api.github.com/repos/bitcoin/bitcoin/releases/latest" | grep -Po '"tag_name": "v\K.*?(?=")')
  OLD_BITCOIN_VERSION=$(gosu bitcoin bitcoind -version | sed -n 's/^Bitcoin Core version v\([0-9]*\.[0-9]*\).[0-9]*$/\1/p')
  if [ "$OLD_BITCOIN_VERSION" != "$NEW_BITCOIN_VERSION" ]; then
    # Keys from https://github.com/bitcoin/bitcoin/blob/master/contrib/verify-commits/trusted-keys
    TRUSTED_KEYS="71A3B16735405025D447E8F274810B012346C9A6 B8B3F1C0E58C15DB6A81D30C3648A882F4316B9B E777299FC265DD04793070EB944D35F9AC3DB76A D1DBF2C4B96F2DEBF4C16654410108112E7EA81F 152812300785C96444D3334D17565732E08E5E41 6B002C6EA3F91B1B0DF0C9BC8F617F1200A6D25C"
    TARGETPLATFORM=x86_64-linux-gnu
    rm -rf /home/bitcoin/.bitcoin/bin
    curl -SLO $CURLOPTS https://bitcoincore.org/bin/bitcoin-core-${NEW_BITCOIN_VERSION}/bitcoin-${NEW_BITCOIN_VERSION}-${TARGETPLATFORM}.tar.gz
    curl -sSLO $CURLOPTS https://bitcoincore.org/bin/bitcoin-core-${NEW_BITCOIN_VERSION}/SHA256SUMS
    curl -sSLO $CURLOPTS https://bitcoincore.org/bin/bitcoin-core-${NEW_BITCOIN_VERSION}/SHA256SUMS.asc
    # FIXME gnupg2 doesn't work with a socks correctly
    gpg1 --keyserver hkps://keyserver.ubuntu.com --keyserver-options "http-proxy=socks4a://$BITCOIND_TOR_IP:9050" --recv-keys $TRUSTED_KEYS
    gpg --status-fd 1 --verify SHA256SUMS.asc 2>/dev/null | awk '$2 == "VALIDSIG" { print $NF }'
    validsigs=$(gpg --status-fd 1 --verify SHA256SUMS.asc 2>/dev/null | awk '$2 == "VALIDSIG" { print $NF }')
    verified=0
    for sig in $validsigs; do
      echo "$validsigs" | fgrep -qx $sig && verified=$((verified + 1))
    done
    if [ $verified -le 3 ]; then
      echo "Cannot verify bitcoin-core tarball with at least 3 trusted keys, exiting." >&2
      exit 1
    fi
    tar -xzf "bitcoin-${NEW_BITCOIN_VERSION}-${TARGETPLATFORM}.tar.gz" --strip-components=1 -C /home/bitcoin/.bitcoin "bitcoin-${NEW_BITCOIN_VERSION}/bin/"
    rm -f *.tar.gz
    rm -f /home/bitcoin/.bitcoin/bin/bitcoin-qt
  fi
  exec gosu bitcoin "$@"
fi

exec "$@"
