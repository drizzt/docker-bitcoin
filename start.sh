#!/usr/bin/env bash

set -euo pipefail

COMMON_OPTS=(--env=ROOTLESS --sysctl=net.ipv6.conf.all.disable_ipv6=1)

CONTAINERS=(bitcoind_tor bitcoind)

DIR="$PWD"

if command -v podman >/dev/null; then
	docker=podman
	(( UID )) && ROOTLESS=1 || ROOTLESS=0
else
	docker=docker
	if (( UID )); then
		if ! $docker info >/dev/null 2>&1; then
			echo "Please re-launch the script as root (or with sudo)." >&2
			exit 1
		fi
	fi
	ROOTLESS=0
fi

if :; then
	for container in "${CONTAINERS[@]}"; do
#		$docker build --pull=true -t "$container" "$container"
		$docker build -t "$container" "$container"
	done
fi

export BITCOIND_RPCPASS=$(openssl rand -base64 32)
export ROOTLESS

for container in "${CONTAINERS[@]}"; do
	$docker stop "docker_bitcoin_$container" &>/dev/null || :
	$docker rm "docker_bitcoin_$container" &>/dev/null || :
done
if (( ROOTLESS )); then
	COMMON_OPTS+=(--pod=bitcoin-pod)
	$docker pod rm -i bitcoin-pod >/dev/null
	$docker pod create --name=bitcoin-pod --share net >/dev/null
else
	# Delete all docker_bitcoin networks
	$docker network ls | grep '\sdocker_bitcoin_' | while read -r net _; do
		$docker network rm "$net" &>/dev/null
	done || :
	# Create public network for bitcoind-tor container
	$docker network create docker_bitcoin_bitcoind_tor_public_net >/dev/null
	# Create (internal) network between bitcoind and bitcoind-tor
	$docker network create --internal docker_bitcoin_bitcoind_tor_net >/dev/null
fi
mkdir -p "$DIR/.bitcoin"
if (( ROOTLESS == 0)); then
	rm -f "$DIR/.bitcoind_tor_ip"
	: > "$DIR/.bitcoind_tor_ip"
	chown 101:101 "$DIR/.bitcoind_tor_ip"
	NET_OPTS=(--volume="$DIR/.bitcoind_tor_ip:/var/lib/tor/.bitcoind_tor_ip:Z" --net=docker_bitcoin_bitcoind_tor_public_net)
fi
$docker create "${COMMON_OPTS[@]}" --name=docker_bitcoin_bitcoind_tor "${NET_OPTS[@]}" bitcoind_tor >/dev/null
if (( ROOTLESS == 0)); then
	$docker network connect docker_bitcoin_bitcoind_tor_net docker_bitcoin_bitcoind_tor
	# FIXME sometimes tor fails to start with podman
	while :; do
		$docker stop docker_bitcoin_bitcoind_tor
		$docker start docker_bitcoin_bitcoind_tor
		while read -r line; do
			case $line in
			*"[notice] Bootstrapped 100% (done): Done")
				break 2
				;;
			esac
		done < <(timeout 120 $docker logs -f docker_bitcoin_bitcoind_tor) || :
	done
	NET_OPTS=(--env=BITCOIND_TOR_IP=$($docker inspect -f "{{.NetworkSettings.Networks.docker_bitcoin_bitcoind_tor_net.IPAddress}}" docker_bitcoin_bitcoind_tor) --volume="$DIR/.bitcoind_tor_ip:/home/bitcoin/.bitcoind_tor_ip:ro,Z" --net=docker_bitcoin_bitcoind_tor_net)
fi
$docker create "${COMMON_OPTS[@]}" --env=BITCOIND_RPCPASS --name=docker_bitcoin_bitcoind --volume="$DIR/.bitcoin:/home/bitcoin/.bitcoin:Z" "${NET_OPTS[@]}" bitcoind >/dev/null

for container in "${CONTAINERS[@]}"; do
	$docker start "docker_bitcoin_$container"
done
