#!/bin/sh

set -ex

hostname -I | awk '{ print $1 }' > ~/.bitcoind_tor_ip

/usr/sbin/rinetd
/usr/bin/tor
